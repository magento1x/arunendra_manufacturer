[Source from magento.stackexchange.com](https://magento.stackexchange.com/questions/87777/how-to-create-module-to-show-all-manufacturers-in-front-end/87806)

Create Files 1.app\etc\modules\Arunendra_Manufacturer.xml

    <?xml version="1.0"?>
    <config>
      <modules>
        <Arunendra_Manufacturer>
          <active>true</active>
          <codePool>local</codePool>
          <version>0.1.0</version>
        </Arunendra_Manufacturer>
      </modules>
    </config>

2.app\code\local\Arunendra\Manufacturer\etc\config.xml

    <?xml version="1.0"?>
    <config>
      <modules>
        <Arunendra_Manufacturer>
          <version>0.1.0</version>
        </Arunendra_Manufacturer>
      </modules>
      <frontend>
        <routers>
          <manufacturer>
            <use>standard</use>
              <args>
                <module>Arunendra_Manufacturer</module>
                <frontName>manufacturer</frontName>
              </args>
          </manufacturer>
        </routers>
            <layout>
              <updates>
                <manufacturer>
                  <file>manufacturer.xml</file>
                </manufacturer>
              </updates>
            </layout>
      </frontend>
      <global>
        <blocks>
          <manufacturer>
            <class>Arunendra_Manufacturer_Block</class>
          </manufacturer>
        </blocks>
      </global>
    </config> 

3.app\code\local\Arunendra\Manufacturer\controllers\IndexController.php

    <?php
    class Arunendra_Manufacturer_IndexController extends Mage_Core_Controller_Front_Action{
        public function IndexAction() {

          $this->loadLayout();   
          $this->getLayout()->getBlock("head")->setTitle($this->__("Titlename"));
                $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
          $breadcrumbs->addCrumb("home", array(
                    "label" => $this->__("Home Page"),
                    "title" => $this->__("Home Page"),
                    "link"  => Mage::getBaseUrl()
               ));

          $breadcrumbs->addCrumb("titlename", array(
                    "label" => $this->__("Titlename"),
                    "title" => $this->__("Titlename")
               ));

          $this->renderLayout(); 

        }
    }

4.app\code\local\Arunendra\Manufacturer\Block\Index.php

    <?php   
    class Arunendra_Manufacturer_Block_Index extends Mage_Core_Block_Template{   
    public function index()
    {

      $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'manufacturer');
    return $attribute;
    }

    }

5.app\design\frontend\theme-package-name\default\layout\manufacturer.xml

    <?xml version="1.0"?>   
    <layout version="0.1.0">   
      <manufacturer_index_index>   
        <reference name="root">   
          <action method="setTemplate"><template>page/1column.phtml</template></action>   
        </reference>   
        <reference name="content">   
          <block type="manufacturer/index" name="manufacturer_index" template="manufacturer/index.phtml"/>   
        </reference>   
      </manufacturer_index_index>   
    </layout>   

6.app\design\frontend\theme-package-name\default\template\manufacturer\index.phtml

        <?php
        $attribute = $this->index();
        foreach ($attribute->getSource()->getAllOptions(true, true) as $instance) {
                  echo  $instance['label'].'<br>';
                  /* you can customize code here */
                  }

    ?>  

Now go to front end and run **www.yoursite.com/manufacturer**